"""
    Fichier : gestion_utilisateur_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les creancier.
"""
import sys

import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.essais_wtf_forms.wtf_forms_creancier_select import DemoFormSelectWTF

"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete
    
    Test : ex. cliquer sur le menu "creancier" puis cliquer sur le bouton "DELETE" d'un "creancier"
    
    Paramètres : sans
    
    But : Effacer(delete) un creancier qui a été sélectionné dans le formulaire "utilisateur_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "creancier/utilisateur_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/demo_select_wtf", methods=['GET', 'POST'])
def demo_select_wtf():
    creancier_selectionne = None
    # Objet formulaire pour montrer une liste déroulante basé sur la table "t_genre"
    form = DemoFormSelectWTF()
    try:
        if request.method == "POST" and form.submit_btn_ok_dplist_genre.data:

            if form.submit_btn_ok_dplist_genre.data:
                print("creancier sélectionné : ",
                      form.fk_creancier_wtf_demo_demo.data)
                creancier_selectionne = form.fk_creancier_wtf_demo.data
                form.fk_creancier_wtf_demo.choices = session['creancier_val_list_dropdown']

        if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_creancier_afficher = """SELECT id_creancier, raison_sociale FROM t_creancier ORDER BY id_creancier ASC"""
                mc_afficher.execute(strsql_creancier_afficher)

            data_genres = mc_afficher.fetchall()
            print("demo_select_wtf data_genres ", data_genres, " Type : ", type(data_genres))

            """
                Préparer les valeurs pour la liste déroulante de l'objet "form"
                la liste déroulante est définie dans le "wtf_forms_creancier_select.py" 
                le formulaire qui utilise la liste déroulante "zzz_essais_om_104/demo_form_select_wtf.html"
            """
            creancier_val_list_dropdown = []
            for i in data_genres:
                creancier_val_list_dropdown = [(i["id_creancier"], i["raison_sociale"]) for i in data_genres]

            # Aussi possible d'avoir un id numérique et un texte en correspondance
            # creancier_val_list_dropdown = [(i["id_genre"], i["intitule_genre"]) for i in data_genres]

            print("creancier_val_list_dropdown ", creancier_val_list_dropdown)

            form.fk_creancier_wtf_demo.choices = creancier_val_list_dropdown
            session['creancier_val_list_dropdown'] = creancier_val_list_dropdown

            creancier_selectionne = form.fk_creancier_wtf_demo.data
            print("creancier choisi dans la liste :", creancier_selectionne)
            session['creancier_selectionne_get'] = creancier_selectionne

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("zzz_essais_om_104/demo_form_select_wtf.html",
                           form=form,
                           creancier_selectionne=creancier_selectionne)
