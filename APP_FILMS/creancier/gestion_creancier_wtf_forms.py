"""
    Fichier : gestion_facture_en_attente_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    raison_sociale_regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$"
    raison_sociale_wtf = StringField("Renseigner la raison sociale ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                   Regexp(raison_sociale_regexp,
                                                                          message="Pas de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    reference_bvr_regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$"
    reference_bvr_wtf = StringField("Renseigner le bvr", validators=[Length(min=10, max=46, message="min 10 max 46"),
                                                                   Regexp(reference_bvr_regexp,
                                                                          message="Uniquement chiffres, lettres et espaces")
                                                                   ])
    iban_regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$"
    iban_wtf = StringField("Renseigner l'IBAN", validators=[Length(min=10, max=46, message="min 10 max 46"),
                                                                   Regexp(iban_regexp,
                                                                          message="Uniquement chiffres, lettres et espaces")
                                                                   ])
    numero_telephone_regexp = "(\+41)\s?(\d{2})\s?(\d{3})\s?(\d{2})\s?(\d{2})"
    numero_telephone_wtf = StringField("Renseigner le numéro de téléphone", validators=[Length(min=10, max=46, message="min 1 max 46"),
                                                                   Regexp(numero_telephone_regexp,
                                                                          message="Numéro de téléphone au format international. Exemple : +41 11 111 11 11")
                                                                   ])
    adresse_wtf = StringField("Renseigner l'adresse physique")

    submit = SubmitField("Enregistrer le nouveau créancier")


class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    raison_sociale_update_regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$"
    raison_sociale_update_wtf = StringField("Renseigner la raison sociale ", validators=[Length(min=2, max=25, message="min 2 max 25"),
                                                                   Regexp(raison_sociale_update_regexp,
                                                                          message="Pas de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    reference_bvr_update_regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$"
    reference_bvr_update_wtf = StringField("Modifier le BVR", validators=[Length(min=10, max=46, message="min 10 max 46"),
                                                                   Regexp(reference_bvr_update_regexp,
                                                                          message="Uniquement chiffres et espaces")
                                                                   ])

    iban_update_regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$"
    iban_update_wtf = StringField("Modifier l'IBAN", validators=[Length(min=10, max=46, message="min 10 max 46"),
                                                                   Regexp(iban_update_regexp,
                                                                          message="Uniquement chiffres et espaces")
                                                                   ])

    numero_telephone_update_regexp = "(\+41)\s?(\d{2})\s?(\d{3})\s?(\d{2})\s?(\d{2})"
    numero_telephone_update_wtf = StringField("Modifier le numéro de téléphone", validators=[Length(min=10, max=46, message="min 1 max 46"),
                                                                   Regexp(numero_telephone_update_regexp,
                                                                          message="Numéro de téléphone au format international. Exemple : +41 11 111 11 11")
                                                                   ])

    adresse_update_wtf = StringField("Modifier l'adresse physique")

    submit = SubmitField("Enregistrer creancier")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_delete_wtf.html"

        nom_creancier_delete_wtf : Champ qui reçoit la valeur du creancier, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "creancier".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_creancier".
    """
    nom_creancier_delete_wtf = StringField("Effacer ce créancier")
    submit_btn_del = SubmitField("Effacer creancier")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
