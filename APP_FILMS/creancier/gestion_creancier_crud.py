"""
    Fichier : gestion_utilisateur_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les creancier.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.creancier.gestion_creancier_wtf_forms import FormWTFAjouterGenres
from APP_FILMS.creancier.gestion_creancier_wtf_forms import FormWTFDeleteGenre
from APP_FILMS.creancier.gestion_creancier_wtf_forms import FormWTFUpdateGenre

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /creancier_afficher
    
    Test : ex : http://127.0.0.1:5005/creancier_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_creancier_sel = 0 >> tous les creancier.
                id_creancier_sel = "n" affiche le creancier dont l'id est "n"
"""


@obj_mon_application.route("/creancier_afficher/<string:order_by>/<int:id_creancier_sel>", methods=['GET', 'POST'])
def creancier_afficher(order_by, id_creancier_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion creancier ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_creancier_sel == 0:
                    strsql_creancier_afficher = """SELECT id_creancier, raison_sociale, reference_bvr, iban, numero_telephone, adresse FROM t_creancier ORDER BY id_creancier ASC"""
                    mc_afficher.execute(strsql_creancier_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_creancier"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du creancier sélectionné avec un nom de variable
                    valeur_id_creancier_selected_dictionnaire = {"value_id_creancier_selected": id_creancier_sel}
                    strsql_creancier_afficher = """SELECT id_creancier, raison_sociale, reference_bvr, iban, numero_telephone, adresse FROM t_creancier WHERE id_creancier = %(value_id_creancier_selected)s"""

                    mc_afficher.execute(strsql_creancier_afficher, valeur_id_creancier_selected_dictionnaire)
                else:
                    strsql_creancier_afficher = """SELECT id_creancier, raison_sociale, reference_bvr, iban, numero_telephone, adresse FROM t_creancier ORDER BY id_creancier DESC"""

                    mc_afficher.execute(strsql_creancier_afficher)

                data_genres = mc_afficher.fetchall()

                print("data_genres ", data_genres, " Type : ", type(data_genres))

                # Différencier les messages si la table est vide.
                if not data_genres and id_creancier_sel == 0:
                    flash("""La table "t_creancier" est vide. !!""", "warning")
                elif not data_genres and id_creancier_sel > 0:
                    # Si l'utilisateur change l'id_creancier dans l'URL et que le creancier n'existe pas,
                    flash(f"Créancier inexistant", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_creancier" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateur.
                    flash(f"Créanciers affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. creancier_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} creancier_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("creancier/creancier_afficher.html", data=data_genres)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /genres_ajouter
    
    Test : ex : http://127.0.0.1:5005/genres_ajouter
    
    Paramètres : sans
    
    But : Ajouter un creancier pour un facture
    
    Remarque :  Dans le champ "name_genre_html" du formulaire "creancier/genres_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/creancier_ajouter", methods=['GET', 'POST'])
def creancier_ajouter_wtf():
    form = FormWTFAjouterGenres()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion creancier ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                raison_sociale_wtf = form.raison_sociale_wtf.data
                reference_bvr_wtf = form.reference_bvr_wtf.data
                iban_wtf = form.iban_wtf.data
                numero_telephone_wtf = form.numero_telephone_wtf.data
                adresse_wtf = form.adresse_wtf.data

                valeurs_insertion_dictionnaire = {"value_raison_sociale_wtf": raison_sociale_wtf,
                                                  "value_reference_bvr_wtf": reference_bvr_wtf,
                                                  "value_iban_wtf": iban_wtf,
                                                  "value_numero_telephone_wtf": numero_telephone_wtf,
                                                  "value_adresse_wtf": adresse_wtf}

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_creancier = """INSERT INTO t_creancier (id_creancier, raison_sociale, reference_bvr, iban, numero_telephone, adresse) 
                VALUES (NULL,%(value_raison_sociale_wtf)s, %(value_reference_bvr_wtf)s, %(value_iban_wtf)s, %(value_numero_telephone_wtf)s, %(value_adresse_wtf)s)"""

                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_creancier, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('creancier_afficher', order_by='DESC', id_creancier_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_genre_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_genre_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion creancier CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("creancier/creancier_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /creancier_update
    
    Test : ex cliquer sur le menu "creancier" puis cliquer sur le bouton "EDIT" d'un "creancier"
    
    Paramètres : sans
    
    But : Editer(update) un creancier qui a été sélectionné dans le formulaire "facture_en_attente_afficher.html"
    
    Remarque :  Dans le champ "nom_creancier_update_wtf" du formulaire "creancier/facture_en_attente_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/creancier_update", methods=['GET', 'POST'])
def creancier_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_creancier"
    id_creancier_update = request.values['id_creancier_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateGenre()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "facture_en_attente_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            raison_sociale_update = form_update.raison_sociale_update_wtf.data
            reference_bvr_update = form_update.reference_bvr_update_wtf.data
            iban_update = form_update.iban_update_wtf.data
            numero_telephone_update = form_update.numero_telephone_update_wtf.data
            adresse_update = form_update.adresse_update_wtf.data

            raison_sociale_update = raison_sociale_update.lower()
            adresse_update = adresse_update.lower()

            valeur_update_dictionnaire = {"value_id_creancier": id_creancier_update,
                                          "value_raison_sociale": raison_sociale_update,
                                          "value_reference_bvr": reference_bvr_update,
                                          "value_iban": iban_update,
                                          "value_numero_telephone": numero_telephone_update,
                                          "value_adresse": adresse_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_creancier SET raison_sociale = %(value_raison_sociale)s WHERE id_creancier = %(value_id_creancier)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_creancier SET reference_bvr = %(value_reference_bvr)s WHERE id_creancier = %(value_id_creancier)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_creancier SET iban = %(value_iban)s WHERE id_creancier = %(value_id_creancier)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_creancier SET numero_telephone = %(value_numero_telephone)s WHERE id_creancier = %(value_id_creancier)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_creancier SET adresse = %(value_adresse)s WHERE id_creancier = %(value_id_creancier)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_creancie_update"
            return redirect(url_for('creancier_afficher', order_by="ASC", id_creancier_sel=id_creancier_update))

            #Revenir à l'affichage des créanciers


        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_creancier" et "raison_sociale" de la "t_creancier"
            str_sql_id_creancier = "SELECT id_creancier, raison_sociale, reference_bvr, iban, numero_telephone, adresse FROM t_creancier WHERE id_creancier = %(value_id_creancier)s"
            valeur_select_dictionnaire = {"value_id_creancier": id_creancier_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_creancier, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom creancier" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " creancier ",
                  data_nom_genre["raison_sociale"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "facture_en_attente_update_wtf.html"
            form_update.raison_sociale_update_wtf.data = data_nom_genre["raison_sociale"]
            form_update.reference_bvr_update_wtf.data = data_nom_genre["reference_bvr"]
            form_update.iban_update_wtf.data = data_nom_genre["iban"]
            form_update.numero_telephone_update_wtf.data = data_nom_genre["numero_telephone"]
            form_update.adresse_update_wtf.data = data_nom_genre["adresse"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans creancier_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans creancier_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans creancier_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans creancier_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("creancier/creancier_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete
    
    Test : ex. cliquer sur le menu "creancier" puis cliquer sur le bouton "DELETE" d'un "creancier"
    
    Paramètres : sans
    
    But : Effacer(delete) un creancier qui a été sélectionné dans le formulaire "utilisateur_afficher.html"
    
    Remarque :  Dans le champ "nom_creancier_delete_wtf" du formulaire "creancier/utilisateur_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/genre_delete", methods=['GET', 'POST'])
def creancier_delete_wtf():
    data_films_attribue_genre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_creancier"
    id_creancier_delete = request.values['id_creancier_btn_delete_html']

    # Objet formulaire pour effacer le creancier sélectionné.
    form_delete = FormWTFDeleteGenre()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("creancier_afficher", order_by="ASC", id_creancier_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "creancier/facture_en_attente_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_genre_delete = session['data_films_attribue_genre_delete']
                print("data_films_attribue_genre_delete ", data_films_attribue_genre_delete)

                flash(f"Effacer le creancier de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer creancier" qui va irrémédiablement EFFACER le creancier
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_creancier": id_creancier_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_genre = """DELETE FROM t_utilisateur_avoir_creancier WHERE fk_creancier = %(value_id_creancier)s;
                                                DELETE FROM t_facture WHERE fk_creancier = %(value_id_creancier)s"""

                str_sql_delete_idgenre = """DELETE FROM t_creancier WHERE id_creancier = %(value_id_creancier)s"""
                # Manière brutale d'effacer d'abord la "fk_creancier", même si elle n'existe pas dans la "t_creancier"
                # Ensuite on peut effacer le creancier vu qu'il n'est plus "lié" (INNODB) dans la "t_creancier"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_genre, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idgenre, valeur_delete_dictionnaire)

                flash(f"Créancier définitivement effacé !!", "success")
                print(f"Créancier définitivement effacé !!")

                # afficher les données
                return redirect(url_for('creancier_afficher', order_by="ASC", id_creancier_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_creancier": id_creancier_delete}
            print(id_creancier_delete, type(id_creancier_delete))
#SELECT id_facture, fk_creancier, numero_facture, reference_bvr, montant_facture, motif_facturation, echeance_facture
            # Requête qui affiche tous les facture qui ont le creancier que l'utilisateur veut effacer
            # Requête qui affiche toutes les factures que le créancier avait et que l'utilisateur veut supprimer
            str_sql_genres_films_delete = """SELECT id_facture, numero_facture, reference_bvr_facture,montant_facture, motif_facturation, echeance_facture
                                            FROM t_facture 
                                            INNER JOIN t_facture_en_Attente ON t_facture.id_facture = t_facture_en_Attente.id_facture_en_Attente
                                            WHERE fk_creancier = %(value_id_creancier)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_films_attribue_genre_delete = mybd_curseur.fetchall()
            print("data_films_attribue_genre_delete...", data_films_attribue_genre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "creancier/facture_en_attente_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_genre_delete'] = data_films_attribue_genre_delete

            # Opération sur la BD pour récupérer "id_creancier" et "raison_sociale" de la "t_creancier"
            str_sql_id_creancier = "SELECT id_creancier, raison_sociale FROM t_creancier WHERE id_creancier = %(value_id_creancier)s"

            mybd_curseur.execute(str_sql_id_creancier, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom creancier" pour l'action DELETE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " creancier ",
                  data_nom_genre["raison_sociale"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "utilisateur_delete_wtf.html"
            form_delete.nom_creancier_delete_wtf.data = data_nom_genre["raison_sociale"]

            # Le bouton pour l'action "DELETE" dans le form. "facture_en_attente_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans creancier_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans creancier_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans creancier_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans creancier_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("creancier/creancier_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_genre_delete)
