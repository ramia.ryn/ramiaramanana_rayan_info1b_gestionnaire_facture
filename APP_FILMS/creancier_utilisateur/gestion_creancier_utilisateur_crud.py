"""
    Fichier : gestion_creancier_utilisateur_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les facture et les creancier.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Nom : utilisateur_creancier_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /utilisateur_creancier_afficher
    
    But : Afficher les facture avec les creancier associés pour chaque facture.
    
    Paramètres : id_genre_sel = 0 >> tous les facture.
                 id_genre_sel = "n" affiche le facture dont l'id est "n"
                 
"""


@obj_mon_application.route("/creancier_utilisateur_afficher/<int:id_utilisateur_sel>", methods=['GET', 'POST'])
def utilisateur_creancier_afficher(id_utilisateur_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_utilisateur_creancier_afficher:
                code, msg = Exception_init_utilisateur_creancier_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_utilisateur_creancier_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_utilisateur_creancier_afficher.args[0]} , "
                      f"{Exception_init_utilisateur_creancier_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_genres_films_afficher_data = """SELECT id_utilisateur, nom_utilisateur, mot_de_passe, banque_utilisateur, numero_bancaire, cryptogramme_visuel,
                                                            date_expiration_carte,
                                                            GROUP_CONCAT(raison_sociale) as GenresFilms FROM t_utilisateur_avoir_creancier
                                                            RIGHT JOIN t_utilisateur ON t_utilisateur.id_utilisateur = t_utilisateur_avoir_creancier.fk_utilisateur
                                                            LEFT JOIN t_creancier ON t_creancier.id_creancier = t_utilisateur_avoir_creancier.fk_creancier
                                                            GROUP BY id_utilisateur"""
                if id_utilisateur_sel == 0:
                    # le paramètre 0 permet d'afficher tous les facture
                    # Sinon le paramètre représente la valeur de l'id du facture
                    mc_afficher.execute(strsql_genres_films_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du facture sélectionné avec un nom de variable
                    valeur_id_utilisateur_selected_dictionnaire = {"value_id_utilisateur_selected": id_utilisateur_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_genres_films_afficher_data += """ HAVING id_utilisateur= %(value_id_utilisateur_selected)s"""

                    mc_afficher.execute(strsql_genres_films_afficher_data, valeur_id_utilisateur_selected_dictionnaire)

                # Récupère les données de la requête.
                data_genres_films_afficher = mc_afficher.fetchall()
                print("data_genres ", data_genres_films_afficher, " Type : ", type(data_genres_films_afficher))

                # Différencier les messages.
                if not data_genres_films_afficher and id_utilisateur_sel == 0:
                    flash("""La table "t_film" est vide. !""", "warning")
                elif not data_genres_films_afficher and id_utilisateur_sel > 0:
                    # Si l'utilisateur change l'id_film dans l'URL et qu'il ne correspond à aucun facture
                    flash(f"Le facture {id_utilisateur_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données facture et creancier affichés !!", "success")

        except Exception as Exception_utilisateur_creancier_afficher:
            code, msg = Exception_utilisateur_creancier_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception utilisateur_creancier_afficher : {sys.exc_info()[0]} "
                  f"{Exception_utilisateur_creancier_afficher.args[0]} , "
                  f"{Exception_utilisateur_creancier_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("creancier_utilisateur/creancier_utilisateur_afficher.html", data=data_genres_films_afficher)


"""
    nom: edit_genre_film_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les creancier du facture sélectionné par le bouton "MODIFIER" de "utilisateur_creancier_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les creancier contenus dans la "t_genre".
    2) Les creancier attribués au facture selectionné.
    3) Les creancier non-attribués au facture sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_genre_film_selected", methods=['GET', 'POST'])
def edit_genre_film_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_creancier_afficher = """SELECT id_utilisateur, nom_utilisateur, banque_utilisateur FROM t_utilisateur ORDER BY id_utilisateur ASC"""
                mc_afficher.execute(strsql_creancier_afficher)
            data_genres_all = mc_afficher.fetchall()
            print("dans edit_genre_film_selected ---> data_genres_all", data_genres_all)

            # Récupère la valeur de "id_film" du formulaire html "utilisateur_creancier_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_film"
            # grâce à la variable "id_utilisateur_creancier_edit_html" dans le fichier "utilisateur_creancier_afficher.html"
            # href="{{ url_for('edit_genre_film_selected', id_utilisateur_creancier_edit_html=row.id_film) }}"
            id_utilisateur_creancier_edit = request.values['id_creancier_utilisateur_edit_html']

            # Mémorise l'id du facture dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_utilisateur_creancier_edit'] = id_utilisateur_creancier_edit

            # Constitution d'un dictionnaire pour associer l'id du facture sélectionné avec un nom de variable
            valeur_id_utilisateur_selected_dictionnaire = {"value_id_utilisateur_selected": id_utilisateur_creancier_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction genres_films_afficher_data
            # 1) Sélection du facture choisi
            # 2) Sélection des creancier "déjà" attribués pour le facture.
            # 3) Sélection des creancier "pas encore" attribués pour le facture choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "genres_films_afficher_data"
            data_genre_film_selected, data_genres_films_non_attribues, data_genres_films_attribues = \
                genres_films_afficher_data(valeur_id_utilisateur_selected_dictionnaire)

            print(data_genre_film_selected)
            lst_data_film_selected = [item['id_utilisateur'] for item in data_genre_film_selected]
            print("lst_data_film_selected  ", lst_data_film_selected,
                  type(lst_data_film_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les creancier qui ne sont pas encore sélectionnés.
            lst_data_genres_films_non_attribues = [item['id_creancier'] for item in data_genres_films_non_attribues]
            session['session_lst_data_genres_films_non_attribues'] = lst_data_genres_films_non_attribues
            print("lst_data_genres_films_non_attribues  ", lst_data_genres_films_non_attribues,
                  type(lst_data_genres_films_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les creancier qui sont déjà sélectionnés.
            lst_data_genres_films_old_attribues = [item['id_creancier'] for item in data_genres_films_attribues]
            session['session_lst_data_genres_films_old_attribues'] = lst_data_genres_films_old_attribues
            print("lst_data_genres_films_old_attribues  ", lst_data_genres_films_old_attribues,
                  type(lst_data_genres_films_old_attribues))

            print(" data data_genre_film_selected", data_genre_film_selected, "type ", type(data_genre_film_selected))
            print(" data data_genres_films_non_attribues ", data_genres_films_non_attribues, "type ",
                  type(data_genres_films_non_attribues))
            print(" data_genres_films_attribues ", data_genres_films_attribues, "type ",
                  type(data_genres_films_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_genres_films_non_attribues = [item['raison_sociale'] for item in data_genres_films_non_attribues]
            print("lst_all_genres gf_edit_genre_film_selected ", lst_data_genres_films_non_attribues,
                  type(lst_data_genres_films_non_attribues))

        except Exception as Exception_edit_genre_film_selected:
            code, msg = Exception_edit_genre_film_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_genre_film_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_genre_film_selected.args[0]} , "
                  f"{Exception_edit_genre_film_selected}", "danger")

    return render_template("creancier_utilisateur/creancier_utilisateur_modifier_tags_dropbox.html",
                           data_genres=data_genres_all,
                           data_film_selected=data_genre_film_selected,
                           data_genres_attribues=data_genres_films_attribues,
                           data_genres_non_attribues=data_genres_films_non_attribues)


"""
    nom: update_genre_film_selected

    Récupère la liste de tous les creancier du facture sélectionné par le bouton "MODIFIER" de "utilisateur_creancier_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les creancier contenus dans la "t_genre".
    2) Les creancier attribués au facture selectionné.
    3) Les creancier non-attribués au facture sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_genre_film_selected", methods=['GET', 'POST'])
def update_genre_film_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du facture sélectionné
            id_utilisateur_selected = session['session_id_utilisateur_creancier_edit']
            print("session['session_id_utilisateur_creancier_edit'] ", session['session_id_utilisateur_creancier_edit'])

            # Récupère la liste des creancier qui ne sont pas associés au facture sélectionné.
            old_lst_data_genres_films_non_attribues = session['session_lst_data_genres_films_non_attribues']
            print("old_lst_data_genres_films_non_attribues ", old_lst_data_genres_films_non_attribues)

            # Récupère la liste des creancier qui sont associés au facture sélectionné.
            old_lst_data_genres_films_attribues = session['session_lst_data_genres_films_old_attribues']
            print("old_lst_data_genres_films_old_attribues ", old_lst_data_genres_films_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme creancier dans le composant "tags-selector-tagselect"
            # dans le fichier "genres_films_modifier_tags_dropbox.html"
            new_lst_str_genres_films = request.form.getlist('name_select_tags')
            print("new_lst_str_genres_films ", new_lst_str_genres_films)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_genre_film_old = list(map(int, new_lst_str_genres_films))
            print("new_lst_genre_film ", new_lst_int_genre_film_old, "type new_lst_genre_film ",
                  type(new_lst_int_genre_film_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_genre" qui doivent être effacés de la table intermédiaire "t_genre_film".
            lst_diff_genres_delete_b = list(
                set(old_lst_data_genres_films_attribues) - set(new_lst_int_genre_film_old))
            print("lst_diff_genres_delete_b ", lst_diff_genres_delete_b)

            # Une liste de "id_genre" qui doivent être ajoutés à la "t_genre_film"
            lst_diff_genres_insert_a = list(
                set(new_lst_int_genre_film_old) - set(old_lst_data_genres_films_attribues))
            print("lst_diff_genres_insert_a ", lst_diff_genres_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_film"/"id_film" et "fk_genre"/"id_genre" dans la "t_genre_film"
            strsql_insert_genre_film = """INSERT INTO t_utilisateur_avoir_creancier (id_utilisateur_avoir_creancier, fk_creancier, fk_utilisateur)
                                                    VALUES (NULL, %(value_fk_creancier)s, %(value_fk_utilisateur)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_genre_film = """DELETE FROM t_utilisateur_avoir_creancier WHERE fk_creancier = %(value_fk_creancier)s AND fk_utilisateur = %(value_fk_utilisateur)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le facture sélectionné, parcourir la liste des creancier à INSÉRER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_creancier_ins in lst_diff_genres_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du facture sélectionné avec un nom de variable
                    # et "id_creancier_ins" (l'id du creancier dans la liste) associé à une variable.
                    valeurs_film_sel_genre_sel_dictionnaire = {"value_fk_utilisateur": id_utilisateur_selected,
                                                               "value_fk_creancier": id_creancier_ins}

                    mconn_bd.mabd_execute(strsql_insert_genre_film, valeurs_film_sel_genre_sel_dictionnaire)

                # Pour le facture sélectionné, parcourir la liste des creancier à EFFACER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_creancier_del in lst_diff_genres_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du facture sélectionné avec un nom de variable
                    # et "id_creancier_del" (l'id du creancier dans la liste) associé à une variable.
                    valeurs_film_sel_genre_sel_dictionnaire = {"value_fk_utilisateur": id_utilisateur_selected,
                                                               "value_fk_creancier": id_creancier_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_genre_film, valeurs_film_sel_genre_sel_dictionnaire)

        except Exception as Exception_update_genre_film_selected:
            code, msg = Exception_update_genre_film_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_genre_film_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_genre_film_selected.args[0]} , "
                  f"{Exception_update_genre_film_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_genre_film",
    # on affiche les facture et le(urs) creancier(s) associé(s).
    return redirect(url_for('utilisateur_creancier_afficher', id_utilisateur_sel=id_utilisateur_selected))


"""
    nom: genres_films_afficher_data

    Récupère la liste de tous les creancier du facture sélectionné par le bouton "MODIFIER" de "utilisateur_creancier_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des creancier, ainsi l'utilisateur voit les creancier à disposition

    On signale les erreurs importantes
"""


def genres_films_afficher_data(valeur_id_utilisateur_selected_dict):
    print("valeur_id_utilisateur_selected_dict...", valeur_id_utilisateur_selected_dict)
    try:

        strsql_film_selected = """SELECT id_utilisateur, nom_utilisateur, mot_de_passe, banque_utilisateur, numero_bancaire, cryptogramme_visuel, date_expiration_carte, GROUP_CONCAT(id_utilisateur) as GenresFilms FROM t_utilisateur_avoir_creancier
                                        INNER JOIN t_utilisateur ON t_utilisateur.id_utilisateur = t_utilisateur_avoir_creancier.fk_utilisateur
                                        INNER JOIN t_creancier ON t_creancier.id_creancier = t_utilisateur_avoir_creancier.fk_creancier
                                        WHERE id_utilisateur = %(value_id_utilisateur_selected)s"""

        strsql_genres_films_non_attribues = """SELECT id_creancier, raison_sociale, reference_bvr, iban, numero_telephone, adresse FROM t_creancier WHERE id_creancier not in(SELECT id_creancier as idGenresFilms FROM t_utilisateur_avoir_creancier
                                                    INNER JOIN t_utilisateur ON t_utilisateur.id_utilisateur = t_utilisateur_avoir_creancier.fk_utilisateur
                                                    INNER JOIN t_creancier ON t_creancier.id_creancier = t_utilisateur_avoir_creancier.fk_creancier
                                                    WHERE id_utilisateur = %(value_id_utilisateur_selected)s)"""

        strsql_genres_films_attribues = """SELECT id_utilisateur, id_creancier, raison_sociale FROM t_utilisateur_avoir_creancier
                                            INNER JOIN t_utilisateur ON t_utilisateur.id_utilisateur = t_utilisateur_avoir_creancier.fk_utilisateur
                                            INNER JOIN t_creancier ON t_creancier.id_creancier = t_utilisateur_avoir_creancier.fk_creancier
                                            WHERE id_utilisateur = %(value_id_utilisateur_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_genres_films_non_attribues, valeur_id_utilisateur_selected_dict)
            # Récupère les données de la requête.
            data_genres_films_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("genres_films_afficher_data ----> data_genres_films_non_attribues ", data_genres_films_non_attribues,
                  " Type : ",
                  type(data_genres_films_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_film_selected, valeur_id_utilisateur_selected_dict)
            # Récupère les données de la requête.
            data_film_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_film_selected  ", data_film_selected, " Type : ", type(data_film_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_genres_films_attribues, valeur_id_utilisateur_selected_dict)
            # Récupère les données de la requête.
            data_genres_films_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_genres_films_attribues ", data_genres_films_attribues, " Type : ",
                  type(data_genres_films_attribues))

            # Retourne les données des "SELECT"
            return data_film_selected, data_genres_films_non_attribues, data_genres_films_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans genres_films_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans genres_films_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_genres_films_afficher_data:
        code, msg = IntegrityError_genres_films_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans genres_films_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_genres_films_afficher_data.args[0]} , "
              f"{IntegrityError_genres_films_afficher_data}", "danger")
