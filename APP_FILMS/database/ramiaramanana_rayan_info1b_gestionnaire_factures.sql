-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 18 Avril 2021 à 12:06
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ramiaramanana_rayan_info1b_gestionnaire_factures`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_creancier`
--
DROP DATABASE IF EXISTS ramiaramanana_rayan_info1b_gestionnaire_factures;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS ramiaramanana_rayan_info1b_gestionnaire_factures;

-- Utilisation de cette base de donnée

USE ramiaramanana_rayan_info1b_gestionnaire_factures;
--
-- Base de données :  `ramiaramanana_rayan_info1b_gestionnaire_factures`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_creancier`
--

CREATE TABLE `t_creancier` (
  `id_creancier` int(11) NOT NULL,
  `raison_sociale` varchar(25) NOT NULL,
  `reference_bvr` varchar(46) NOT NULL,
  `iban` varchar(21) NOT NULL,
  `numero_telephone` varchar(20) NOT NULL,
  `adresse` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_creancier`
--

INSERT INTO `t_creancier` (`id_creancier`, `raison_sociale`, `reference_bvr`, `iban`, `numero_telephone`, `adresse`) VALUES
(1, 'micro informatic sa', '33 12340 00000 00000 00001 23152', 'ch2180808001234567890', '244474472', 'en chamard 41a 1442 montagny, yverdon-les-bains'),
(6, 'testfv', '234242342', 'sdfsdf', '123432', '23432dfdsf'),
(7, 'bBHBIIhbsbh SA', '13761623 12312 31CH', 'CH 1231123123', '+41795355852', 'adsdas das das 12');

-- --------------------------------------------------------

--
-- Structure de la table `t_creancier_avoir_facture`
--

CREATE TABLE `t_creancier_avoir_facture` (
  `id_creancier_avoir_facture` int(11) NOT NULL,
  `fk_creancier` int(11) NOT NULL,
  `fk_facture` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_facture`
--

CREATE TABLE `t_facture` (
  `id_facture` int(11) NOT NULL,
  `fk_creancier` int(11) NOT NULL,
  `numero_facture` int(25) NOT NULL,
  `reference_bvr_facture` varchar(60) NOT NULL,
  `montant_facture` float NOT NULL,
  `motif_facturation` text NOT NULL,
  `echeance_facture` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_facture`
--

INSERT INTO `t_facture` (`id_facture`, `fk_creancier`, `numero_facture`, `reference_bvr_facture`, `montant_facture`, `motif_facturation`, `echeance_facture`) VALUES
(13, 1, 456654, '54544545', 54, 'dfdsf', '2005-12-02');

-- --------------------------------------------------------

--
-- Structure de la table `t_facture_en_attente`
--

CREATE TABLE `t_facture_en_attente` (
  `id_facture_en_attente` int(11) NOT NULL,
  `fk_utilisateur` int(11) NOT NULL,
  `fk_facture` int(11) NOT NULL,
  `date_enregistrement_facture` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_facture_en_attente`
--

INSERT INTO `t_facture_en_attente` (`id_facture_en_attente`, `fk_utilisateur`, `fk_facture`, `date_enregistrement_facture`) VALUES
(8, 1, 13, '2021-06-07 22:00:00'),
(14, 1, 13, '2021-06-16 22:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `t_utilisateur`
--

CREATE TABLE `t_utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `nom_utilisateur` varchar(25) NOT NULL,
  `mot_de_passe` varchar(25) NOT NULL,
  `banque_utilisateur` varchar(15) NOT NULL,
  `numero_bancaire` float NOT NULL,
  `cryptogramme_visuel` int(4) NOT NULL,
  `date_expiration_carte` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_utilisateur`
--

INSERT INTO `t_utilisateur` (`id_utilisateur`, `nom_utilisateur`, `mot_de_passe`, `banque_utilisateur`, `numero_bancaire`, `cryptogramme_visuel`, `date_expiration_carte`) VALUES
(1, 'admin', 'Admin', 'ubs', 1234570000, 12, '0000-00-00'),
(2, 'Rayan', '123456789', 'UBS', 987654000, 1234, '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `t_utilisateur_avoir_creancier`
--

CREATE TABLE `t_utilisateur_avoir_creancier` (
  `id_utilisateur_avoir_creancier` int(11) NOT NULL,
  `fk_utilisateur` int(11) NOT NULL,
  `fk_creancier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_utilisateur_avoir_creancier`
--

INSERT INTO `t_utilisateur_avoir_creancier` (`id_utilisateur_avoir_creancier`, `fk_utilisateur`, `fk_creancier`) VALUES
(1, 1, 1),
(3, 1, 7),
(4, 2, 6);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_creancier`
--
ALTER TABLE `t_creancier`
  ADD PRIMARY KEY (`id_creancier`);

--
-- Index pour la table `t_creancier_avoir_facture`
--
ALTER TABLE `t_creancier_avoir_facture`
  ADD PRIMARY KEY (`id_creancier_avoir_facture`),
  ADD KEY `fk_creancier` (`fk_creancier`),
  ADD KEY `fk_facture` (`fk_facture`);

--
-- Index pour la table `t_facture`
--
ALTER TABLE `t_facture`
  ADD PRIMARY KEY (`id_facture`),
  ADD KEY `creancier` (`fk_creancier`);

--
-- Index pour la table `t_facture_en_attente`
--
ALTER TABLE `t_facture_en_attente`
  ADD PRIMARY KEY (`id_facture_en_attente`),
  ADD KEY `fk_utilisateur` (`fk_utilisateur`),
  ADD KEY `fk_facture` (`fk_facture`);

--
-- Index pour la table `t_utilisateur`
--
ALTER TABLE `t_utilisateur`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- Index pour la table `t_utilisateur_avoir_creancier`
--
ALTER TABLE `t_utilisateur_avoir_creancier`
  ADD PRIMARY KEY (`id_utilisateur_avoir_creancier`),
  ADD KEY `fk_utilisateur` (`fk_utilisateur`),
  ADD KEY `fk_creancier` (`fk_creancier`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_creancier`
--
ALTER TABLE `t_creancier`
  MODIFY `id_creancier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_creancier_avoir_facture`
--
ALTER TABLE `t_creancier_avoir_facture`
  MODIFY `id_creancier_avoir_facture` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_facture`
--
ALTER TABLE `t_facture`
  MODIFY `id_facture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `t_facture_en_attente`
--
ALTER TABLE `t_facture_en_attente`
  MODIFY `id_facture_en_attente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `t_utilisateur`
--
ALTER TABLE `t_utilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_utilisateur_avoir_creancier`
--
ALTER TABLE `t_utilisateur_avoir_creancier`
  MODIFY `id_utilisateur_avoir_creancier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_creancier_avoir_facture`
--
ALTER TABLE `t_creancier_avoir_facture`
  ADD CONSTRAINT `t_creancier_avoir_facture_ibfk_1` FOREIGN KEY (`fk_creancier`) REFERENCES `t_creancier` (`id_creancier`),
  ADD CONSTRAINT `t_creancier_avoir_facture_ibfk_2` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`);

--
-- Contraintes pour la table `t_facture`
--
ALTER TABLE `t_facture`
  ADD CONSTRAINT `t_facture_ibfk_1` FOREIGN KEY (`fk_creancier`) REFERENCES `t_creancier` (`id_creancier`);

--
-- Contraintes pour la table `t_facture_en_attente`
--
ALTER TABLE `t_facture_en_attente`
  ADD CONSTRAINT `t_facture_en_attente_ibfk_1` FOREIGN KEY (`fk_utilisateur`) REFERENCES `t_utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `t_facture_en_attente_ibfk_2` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`);

--
-- Contraintes pour la table `t_utilisateur_avoir_creancier`
--
ALTER TABLE `t_utilisateur_avoir_creancier`
  ADD CONSTRAINT `t_utilisateur_avoir_creancier_ibfk_1` FOREIGN KEY (`fk_utilisateur`) REFERENCES `t_utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `t_utilisateur_avoir_creancier_ibfk_2` FOREIGN KEY (`fk_creancier`) REFERENCES `t_creancier` (`id_creancier`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
