"""
    Fichier : gestion_facture_en_attente_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms import SelectField
from wtforms.fields.html5 import DateField

class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    fk_creancier_wtf = StringField("Renseigner l'ID du créancier")
    numero_facture_wtf = StringField("Renseigner le numéro de la facture")
    reference_bvr_facture_wtf = StringField("Renseigner la référence BVR")
    montant_facture_wtf = StringField("Renseigner le montant (CHF)")
    motif_facturation_wtf = StringField("Renseigner le motif de facturation")
    echeance_facture_wtf = DateField("Echéance de la facture", format="%Y-%m-%d")

    submit = SubmitField("Enregistrer la facture")

class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    fk_creancier_update_wtf = StringField("Renseigner l'ID du créancier")
    numero_facture_update_wtf = StringField("Modifier le BVR")
    reference_bvr_facture_update_wtf = StringField("Modifier l'IBAN")
    montant_facture_update_wtf = StringField("Modifier le numéro de téléphone")
    motif_facturation_update_wtf = StringField("Modifier le motif de facturation")
    echeance_facture_update_wtf = DateField("Echéance de la facture", format="%Y-%m-%d")

    submit = SubmitField("Enregistrer la modification")

class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_facture".
    """
    facture_delete_wtf = StringField("Effacer cette facture possèdant le numéro suivant ?")
    submit_btn_del = SubmitField("Effacer facture")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
