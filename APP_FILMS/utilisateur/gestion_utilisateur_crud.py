"""
    Fichier : gestion_utilisateur_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les utilisateur.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.utilisateur.gestion_utilisateur_wtf_forms import FormWTFAjouterGenres
from APP_FILMS.utilisateur.gestion_utilisateur_wtf_forms import FormWTFDeleteGenre
from APP_FILMS.utilisateur.gestion_utilisateur_wtf_forms import FormWTFUpdateGenre

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /utilisateur_afficher
    
    Test : ex : http://127.0.0.1:5005/utilisateur_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_utilisateur_sel = 0 >> tous les utilisateur.
                id_utilisateur_sel = "n" affiche le utilisateur dont l'id est "n"
"""


@obj_mon_application.route("/utilisateur_afficher/<string:order_by>/<int:id_utilisateur_sel>", methods=['GET', 'POST'])
def utilisateur_afficher(order_by, id_utilisateur_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion utilisateur ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_utilisateur_sel == 0:
                    strsql_utilisateur_afficher = """SELECT id_utilisateur, nom_utilisateur, mot_de_passe, banque_utilisateur, numero_bancaire, cryptogramme_visuel, date_expiration_carte FROM t_utilisateur ORDER BY id_utilisateur ASC"""
                    mc_afficher.execute(strsql_utilisateur_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_utilisateur"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du utilisateur sélectionné avec un nom de variable
                    valeur_id_utilisateur_selected_dictionnaire = {"value_id_utilisateur_selected": id_utilisateur_sel}
                    strsql_utilisateur_afficher = """SELECT id_utilisateur, nom_utilisateur, mot_de_passe, banque_utilisateur, numero_bancaire, cryptogramme_visuel, date_expiration_carte FROM t_utilisateur WHERE id_utilisateur = %(value_id_utilisateur_selected)s"""

                    mc_afficher.execute(strsql_utilisateur_afficher, valeur_id_utilisateur_selected_dictionnaire)
                else:
                    strsql_utilisateur_afficher = """SELECT id_utilisateur, nom_utilisateur, mot_de_passe, banque_utilisateur, numero_bancaire, cryptogramme_visuel, date_expiration_carte FROM t_utilisateur ORDER BY id_utilisateur DESC"""

                    mc_afficher.execute(strsql_utilisateur_afficher)

                data_genres = mc_afficher.fetchall()

                print("data_genres ", data_genres, " Type : ", type(data_genres))

                # Différencier les messages si la table est vide.
                if not data_genres and id_utilisateur_sel == 0:
                    flash("""La table "t_utilisateur" est vide. !!""", "warning")
                elif not data_genres and id_utilisateur_sel > 0:
                    # Si l'utilisateur change l'id_utilisateur dans l'URL et que le utilisateur n'existe pas,
                    flash(f"Créancier inexistant", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_utilisateur" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateur.
                    flash(f"Utilisateur(s) affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. utilisateur_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} utilisateur_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("utilisateur/utilisateur_afficher.html", data=data_genres)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /genres_ajouter
    
    Test : ex : http://127.0.0.1:5005/genres_ajouter
    
    Paramètres : sans
    
    But : Ajouter un utilisateur pour un facture
    
    Remarque :  Dans le champ "name_genre_html" du formulaire "utilisateur/genres_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/utilisateur_ajouter", methods=['GET', 'POST'])
def utilisateur_ajouter_wtf():
    form = FormWTFAjouterGenres()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion utilisateur ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                nom_utilisateur_wtf = form.nom_utilisateur_wtf.data
                mot_de_passe_wtf = form.mot_de_passe_wtf.data
                banque_utilisateur_wtf = form.banque_utilisateur_wtf.data
                numero_bancaire_wtf = form.numero_bancaire_wtf.data
                cryptogramme_visuel_wtf = form.cryptogramme_visuel_wtf.data
                date_expiration_carte_wtf = form.date_expiration_carte_wtf.data
                valeurs_insertion_dictionnaire = {"value_nom_utilisateur_wtf": nom_utilisateur_wtf,
                                                  "value_mot_de_passe_wtf": mot_de_passe_wtf,
                                                  "value_banque_utilisateur_wtf": banque_utilisateur_wtf,
                                                  "value_numero_bancaire_wtf":numero_bancaire_wtf,
                                                  "value_cryptogramme_visuel_wtf": cryptogramme_visuel_wtf,
                                                  "value_date_expiration_carte_wtf": date_expiration_carte_wtf}

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_utilisateur = """INSERT INTO t_utilisateur (id_utilisateur, nom_utilisateur, mot_de_passe, banque_utilisateur, numero_bancaire, cryptogramme_visuel, date_expiration_carte) 
                VALUES (NULL,%(value_nom_utilisateur_wtf)s, %(value_mot_de_passe_wtf)s, %(value_banque_utilisateur_wtf)s, %(value_numero_bancaire_wtf)s, %(value_cryptogramme_visuel_wtf)s, %(value_date_expiration_carte_wtf)s)"""

                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_utilisateur, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('utilisateur_afficher', order_by='DESC', id_utilisateur_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_genre_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_genre_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion utilisateur CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("utilisateur/utilisateur_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /utilisateur_update
    
    Test : ex cliquer sur le menu "utilisateur" puis cliquer sur le bouton "EDIT" d'un "utilisateur"
    
    Paramètres : sans
    
    But : Editer(update) un utilisateur qui a été sélectionné dans le formulaire "facture_en_attente_afficher.html"
    
    Remarque :  Dans le champ "nom_utilisateur_update_wtf" du formulaire "utilisateur/facture_en_attente_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/utilisateur_update", methods=['GET', 'POST'])
def utilisateur_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_utilisateur"
    id_utilisateur_update = request.values['id_utilisateur_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateGenre()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "facture_en_attente_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            nom_utilisateur_update = form_update.nom_utilisateur_update_wtf.data
            mot_de_passe_update = form_update.mot_de_passe_update_wtf.data
            banque_utilisateur_update = form_update.banque_utilisateur_update_wtf.data
            numero_bancaire_update = form_update.numero_bancaire_update_wtf.data
            cryptogramme_visuel_update = form_update.cryptogramme_visuel_update_wtf.data
            date_expiration_carte_update = form_update.date_expiration_carte_update_wtf.data

            nom_utilisateur_update = nom_utilisateur_update.lower()
            banque_utilisateur_update = banque_utilisateur_update.lower()

            valeur_update_dictionnaire = {"value_id_utilisateur": id_utilisateur_update,
                                          "value_nom_utilisateur": nom_utilisateur_update,
                                          "value_mot_de_passe": mot_de_passe_update,
                                          "value_banque_utilisateur": banque_utilisateur_update,
                                          "value_numero_bancaire": numero_bancaire_update,
                                          "value_cryptogramme_visuel": cryptogramme_visuel_update,
                                          "value_date_expiration_carte": date_expiration_carte_update,}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_utilisateur SET nom_utilisateur = %(value_nom_utilisateur)s WHERE id_utilisateur = %(value_id_utilisateur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_utilisateur SET mot_de_passe = %(value_mot_de_passe)s WHERE id_utilisateur = %(value_id_utilisateur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_utilisateur SET banque_utilisateur = %(value_banque_utilisateur)s WHERE id_utilisateur = %(value_id_utilisateur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_utilisateur SET numero_bancaire = %(value_numero_bancaire)s WHERE id_utilisateur = %(value_id_utilisateur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_utilisateur SET cryptogramme_visuel = %(value_cryptogramme_visuel)s WHERE id_utilisateur = %(value_id_utilisateur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_utilisateur SET date_expiration_carte = %(value_date_expiration_carte)s WHERE id_utilisateur = %(value_id_utilisateur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_creancie_update"
            return redirect(url_for('utilisateur_afficher', order_by="ASC", id_utilisateur_sel=id_utilisateur_update))

            #Revenir à l'affichage des créanciers


        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_utilisateur" et "raison_sociale" de la "t_utilisateur"
            str_sql_id_utilisateur = """SELECT id_utilisateur, nom_utilisateur, mot_de_passe, banque_utilisateur, numero_bancaire, cryptogramme_visuel, date_expiration_carte FROM t_utilisateur WHERE id_utilisateur = %(value_id_utilisateur)s"""
            valeur_select_dictionnaire = {"value_id_utilisateur": id_utilisateur_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_utilisateur, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom utilisateur" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " utilisateur ",
                  data_nom_genre["nom_utilisateur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "facture_en_attente_update_wtf.html"
            form_update.nom_utilisateur_update_wtf.data = data_nom_genre["nom_utilisateur"]
            form_update.mot_de_passe_update_wtf.data = data_nom_genre["mot_de_passe"]
            form_update.banque_utilisateur_update_wtf.data = data_nom_genre["banque_utilisateur"]
            form_update.numero_bancaire_update_wtf.data = data_nom_genre["numero_bancaire"]
            form_update.cryptogramme_visuel_update_wtf.data = data_nom_genre["cryptogramme_visuel"]
            form_update.date_expiration_carte_update_wtf.data = data_nom_genre["date_expiration_carte"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans utilisateur_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans utilisateur_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans utilisateur_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans utilisateur_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("utilisateur/utilisateur_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete
    
    Test : ex. cliquer sur le menu "utilisateur" puis cliquer sur le bouton "DELETE" d'un "utilisateur"
    
    Paramètres : sans
    
    But : Effacer(delete) un utilisateur qui a été sélectionné dans le formulaire "utilisateur_afficher.html"
    
    Remarque :  Dans le champ "nom_utilisateur_delete_wtf" du formulaire "utilisateur/utilisateur_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/utilisateur_delete", methods=['GET', 'POST'])
def utilisateur_delete_wtf():
    data_films_attribue_genre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_utilisateur"
    id_utilisateur_delete = request.values['id_utilisateur_btn_delete_html']

    # Objet formulaire pour effacer le utilisateur sélectionné.
    form_delete = FormWTFDeleteGenre()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("utilisateur_afficher", order_by="ASC", id_utilisateur_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "utilisateur/facture_en_attente_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_genre_delete = session['data_films_attribue_genre_delete']
                print("data_films_attribue_genre_delete ", data_films_attribue_genre_delete)

                flash(f"Effacer le utilisateur de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer utilisateur" qui va irrémédiablement EFFACER le utilisateur
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_utilisateur": id_utilisateur_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_genre = """DELETE FROM t_utilisateur_avoir_creancier WHERE fk_utilisateur = %(value_id_utilisateur)s;
                                                DELETE FROM t_facture_en_attente WHERE fk_utilisateur = %(value_id_utilisateur)s"""
                str_sql_delete_idgenre = """DELETE FROM t_utilisateur WHERE id_utilisateur = %(value_id_utilisateur)s"""
                # Manière brutale d'effacer d'abord la "fk_utilisateur", même si elle n'existe pas dans la "t_utilisateur"
                # Ensuite on peut effacer le utilisateur vu qu'il n'est plus "lié" (INNODB) dans la "t_utilisateur"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_genre, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idgenre, valeur_delete_dictionnaire)

                flash(f"Créancier définitivement effacé !!", "success")
                print(f"Créancier définitivement effacé !!")

                # afficher les données
                return redirect(url_for('utilisateur_afficher', order_by="ASC", id_utilisateur_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_utilisateur": id_utilisateur_delete}
            print(id_utilisateur_delete, type(id_utilisateur_delete))
#SELECT id_facture, fk_utilisateur, numero_facture, reference_bvr, montant_facture, motif_facturation, echeance_facture
            # Requête qui affiche tous les facture qui ont le utilisateur que l'utilisateur veut effacer
            # Requête qui affiche toutes les factures que le créancier avait et que l'utilisateur veut supprimer
            str_sql_genres_films_delete = """SELECT t_facture_en_attente.date_enregistrement_facture,t_facture_en_attente.fk_facture, t_utilisateur.nom_utilisateur 
            FROM t_utilisateur 
            LEFT JOIN t_facture_en_attente 
            ON t_facture_en_attente.fk_utilisateur = t_utilisateur.id_utilisateur 
            WHERE fk_utilisateur =  %(value_id_utilisateur)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_films_attribue_genre_delete = mybd_curseur.fetchall()
            print("data_films_attribue_genre_delete...", data_films_attribue_genre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "utilisateur/facture_en_attente_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_genre_delete'] = data_films_attribue_genre_delete

            # Opération sur la BD pour récupérer "id_utilisateur" et "raison_sociale" de la "t_utilisateur"
            str_sql_id_utilisateur = "SELECT id_utilisateur, nom_utilisateur FROM t_utilisateur WHERE id_utilisateur = %(value_id_utilisateur)s"

            mybd_curseur.execute(str_sql_id_utilisateur, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom utilisateur" pour l'action DELETE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " utilisateur ",
                  data_nom_genre["nom_utilisateur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "utilisateur_delete_wtf.html"
            form_delete.nom_utilisateur_delete_wtf.data = data_nom_genre["nom_utilisateur"]

            # Le bouton pour l'action "DELETE" dans le form. "facture_en_attente_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans utilisateur_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans utilisateur_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans utilisateur_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans utilisateur_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("utilisateur/utilisateur_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_genre_delete)
