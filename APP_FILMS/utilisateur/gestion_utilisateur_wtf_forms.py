"""
    Fichier : gestion_facture_en_attente_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.fields.html5 import DateField


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_utilisateur_wtf = StringField("Nom d'utilisateur ")
    mot_de_passe_wtf = StringField("Mot de passe de l'utilisateur")
    banque_utilisateur_wtf = StringField("Nom de la banque")
    numero_bancaire_wtf = StringField("Numéro bancaire")
    cryptogramme_visuel_wtf = StringField("Cryptogramme visuel")
    date_expiration_carte_wtf = DateField("Echéance de la facture", format="%Y-%m-%d")

    submit = SubmitField("Enregistrer le nouveau créancier")


class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_utilisateur_update_wtf = StringField("Nom d'utilisateur ")
    mot_de_passe_update_wtf = StringField("Mot de passe de l'utilisateur")
    banque_utilisateur_update_wtf = StringField("Nom de la banque")
    numero_bancaire_update_wtf = StringField("Numéro bancaire")
    cryptogramme_visuel_update_wtf = StringField("Cryptogramme visuel")
    date_expiration_carte_update_wtf = StringField("Modifier la date")

    submit = SubmitField("Enregistrer creancier")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_delete_wtf.html"

        nom_creancier_delete_wtf : Champ qui reçoit la valeur du creancier, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "creancier".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_creancier".
    """
    nom_utilisateur_delete_wtf = StringField("Effacer cette utilisateur ?")
    submit_btn_del = SubmitField("Effacer creancier")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
