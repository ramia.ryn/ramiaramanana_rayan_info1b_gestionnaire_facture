"""
    Fichier : gestion_facture_en_attente_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms import SelectField
from wtforms.fields.html5 import DateField
class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    fk_utilisateur_wtf = StringField("Doit être égale à l'ID de l'utilisateur voulu")
    fk_facture_wtf = StringField("Doit être égale à l'ID de la facture voulu")
    date_enregistrement_facture_wtf = DateField("Date d'enregistrement", format="%Y-%m-%d")

    submit = SubmitField("Enregistrer la facture")

class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    fk_utilisateur_update_wtf = StringField("Modifier l'utilisateur")
    fk_facture_update_wtf = StringField("Modifier la facture")
    date_enregistrement_facture_update_wtf = DateField("Modifier la date d'enregistrement", format="%Y-%m-%d")

    submit = SubmitField("Enregistrer la modification")

class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "facture_en_attente_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_facture".
    """
    facture_en_attente_delete_wtf = StringField("Effacer cette facture possèdant le numéro suivant ?")
    submit_btn_del = SubmitField("Effacer facture")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
