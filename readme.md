Module 104 Exercice du 2021.06.13
---

# Emplacement de la documentation :
* Tous les documents (MCD,MLD,MPD, dictionnaire de données) se trouve dans le répertoire \ramiaramanan_rayan_info1b_gestionnaire_facture\APP_FILMS\database
   de ce projet

##Explication des onglets :

### Créanciers
* Ajouter, Edit et Delete des créanciers
* Regex OK

### Mes factures
* Menu déroulant qui affiche Mes factures et Factures en attentes 
  * Dans "Toutes mes factures" il faut indiquer manuellement l'ID du créancier lors d'un ajout car je n'ai malheureusement pas
  réussi à mettre de liste déroulante.
  Si l'ID du créancier n'est pas indiquer, une erreur en jaune appraitra : Cannot add or update a child row
  * Identique pour les factures en attente, il faut que l' ID de l'utilisateur et de la facture soit des ID existants.

### Utilisateurs
* Menu déroulant qui affiche Gérer les utilisateurs et Utilisateurs/Creancier
  * Gérer les utilisateurs permet le CRUD sur les utilisateurs, le bouton EDIT ne pocède pas de "DateSelect" 
    * Lors du delete, une requete SQL indique si l'utilisateur pocède des facture en attente, s'il en a, l'ID de la facture ainsi que la
date d'échéance est affichée.
  * Utilisateur/Creancier affiche les créanciers des utilisateurs et permet, par le biais du bouton "Modifier" d'ajouter ou de
supprimer un créancier d'un utilisateur, le bouton Gérer les créancier nous oriente sur la page
Afficher du menu Creancier (creancier_afficher), le bouton Gérer les utilisateurs nous orientes
  lui sur la page qui affiche les utilisateurs.

### Remarque :
* Malheureusement j'ai pas réussi à créer de liste déroulante, et comme mon projet est surtout basé sur des tables intermédiaire, cela rend mon projet moins "User Friendly"

